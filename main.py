#!/usr/bin/env python3

__author__ = "Paul Zeinlinger"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
from logzero import logger
import matplotlib
matplotlib.use('AGG')


class MyRandomGenerator:
    def __init__(self, seed, modulus=2**48, a=25214903917, c=11):
        self.__seed = seed
        self.__modulus = modulus
        self.__a = a
        self.__c = c

    def rand(self):
        self.__seed = (self.__a * self.__seed + self.__c) % self.__modulus
        return self.__seed

    def uni01(self):
        return self.rand() / self.__modulus

    def uni(self, imax):
        return int(self.uni01() * imax)


def main(args):
    """ Main entry point of the app """
    logger.info("Importing MatplotLib")
    import matplotlib.pyplot as plt
    import seaborn as sns
    logger.info("Generating {} Pseudo-Random Numbers".format(args.points))
    generator = MyRandomGenerator(args.seed)
    result = [generator.uni01() * 10 for i in range(args.points)]
    logger.info("Plotting Results")
    sns.stripplot([generator.uni(10) for i in range(args.points)], result,
                  jitter=0.3, size=1)
    logger.info("Saving....")
    plt.savefig(args.out)
    logger.info("{} saved".format(args.out))
    logger.info("Done :-)")


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Optional argument flag which defaults to False
    parser.add_argument("-s", "--seed", type=int, default=0)
    parser.add_argument("-o", "--out", default="out.png")
    parser.add_argument("-p", "--points", type=int, default=50000)

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    main(args)
